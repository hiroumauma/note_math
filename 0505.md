# 🐎5-5
## (2)

$$
\lim_{x\to0} \frac{e^x-(x+1)}{x^2(1+x)}
$$

分母の次数３より
$e^x$を３次テイラー展開を行う

$$
e^x = 1+x+\frac{x^2}{2}+\frac{(x\theta)^3}{6} 
\,\,\,\,\,\biggl(0<  ^{\exists}\theta <1\biggr)
$$

よって

$
\begin{aligned} 
\lim_{x\to0} \frac{e^x-(x+1)}{x^2(1+x)}
    & = \lim_{x\to0} \frac{1+x+\frac{x^2}{2}+\frac{(x\theta)^3}{6} -(x+1)}{x^2(1+x)} \\
    & = \lim_{x\to0} \frac{\frac{x^2}{2}+\frac{(x\theta)^3}{6}}{x^2(1+x)} \\
    & = \lim_{x\to0} \frac{\frac{1}{2}+\frac{(\theta)^3}{6}x}{1+x} \\
    & = \frac{1}{2}
\end{aligned}
$
