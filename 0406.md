# 🐎4-6

$$
\left\{
\begin{array}{ll}
x = rcos\theta \\
y = rsin\theta \\
z = f(x,y)
\end{array}
\right.
$$

## (1)

$
\displaystyle
\begin{aligned} 
	z_r
		& = \frac{\partial z}{\partial r} \\
		& = \frac{\partial z}{\partial x}
		    \frac{\partial x}{\partial r}+
		    \frac{\partial z}{\partial y}
		    \frac{\partial y}{\partial r} \\
	    & = z_x cos\theta + z_y sin \theta
\end{aligned}
$

$
\displaystyle
\begin{aligned} 
	z_r
		& = \frac{\partial z}{\partial \theta} \\
		& = \frac{\partial z}{\partial x}
		    \frac{\partial x}{\partial \theta}+
		    \frac{\partial z}{\partial y}
		    \frac{\partial y}{\partial \theta} \\
	    & = z_x(-rsin\theta) + z_y(rcos \theta) \\
	    & = -rz_xsin\theta + rz_y cos\theta
\end{aligned}
$

## (2)

$
\displaystyle
\begin{aligned} 
	z_{\theta\theta}
		& = \frac{\partial}{\partial \theta}\biggl(\frac{\partial z}{\partial \theta}\biggr)\\
		& = (-rz_{x\theta}sin\theta-rz_{x}cos\theta)
		   +(rz_{y\theta}cos\theta -rz_{y}sin\theta) 
\end{aligned}
$

ここで

$
\displaystyle
\begin{aligned} 
	z_{x\theta}
	    & = \frac{\partial z_x}{\partial x}
		    \frac{\partial x}{\partial \theta}+
		    \frac{\partial z_x}{\partial y}
		    \frac{\partial y}{\partial \theta} \\
		& = z_{xx}(-rsin\theta) + z_{xy}(rcos\theta)
\end{aligned}
$

$
\displaystyle
\begin{aligned} 
	z_{y\theta}
	    & = \frac{\partial z_y}{\partial x}
		    \frac{\partial x}{\partial \theta}+
		    \frac{\partial z_y}{\partial y}
		    \frac{\partial y}{\partial \theta} \\
		& = z_{yx}(-rsin\theta) + z_{yy}(rcos\theta)
\end{aligned}
$

これらを代入して

$
\displaystyle
\begin{aligned} 
	z_{\theta\theta}
		& = -rsin\theta\{z_xx(-rsin\theta)+z_{xy}(rcos\theta)\} -rz_xcos\theta \\
		&\,\,\,\,\,\,\,\,\,\,+rcos\theta\{z_yx(-rsin\theta)+z_{yy}(rcos\theta)\} -rz_ysin\theta \\
		& = (r^2sin^2\theta)z_{xx} + (-2r^2sin\theta cos\theta) + (r^2cos^2\theta)z_{yy}+(-rcos\theta)z_x + (-rsin\theta)z_y
\end{aligned}
$

