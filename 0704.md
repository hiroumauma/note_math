# 🐎7-4

### (1)
---
### Point!

- 3Dグラフの概形

$\longrightarrow$停留点を調べる！

---
　  
　  
$$
g(x,y) = (x^2 + y^2)^2 -2x^2 + 2y^2
$$

停留点となる条件は

$
\begin{aligned}
& (g_x=0\,\,\,and\,\,\,g_y=0) \\
\Leftrightarrow & 
\left\{
\begin{array}{ll}
4x^3+4y^2x -4x =0 \\
4x^2y+4y^3+4y = 0
\end{array}
\right. \\
\Leftrightarrow & 
\begin{pmatrix}
x \\
y 
\end{pmatrix} =
\begin{pmatrix}
0 \\
0 
\end{pmatrix} ,
\begin{pmatrix}
0 \\
-1 
\end{pmatrix},
\begin{pmatrix}
-1 \\
0 
\end{pmatrix}
\end{aligned}
$

ここで

$$
\begin{aligned}
H =
\begin{pmatrix}
g_{xx} & g_{xy} \\
g_{xy} & g_{yy}
\end{pmatrix} =
\begin{pmatrix}
12x^2+4y^2-4 & 8xy \\
8xy & 4x^2+12y^2+4
\end{pmatrix}
\end{aligned} 
$$

各候補点における$detH$及び$g_{xx}$の値は
次の通り

|点|$detH$|$f_{xx}$|判定|
|:-:|:----:|:------:|:-:|
|$(0,0)$|$-$|        |峠点|
|$(-1,0)$|$+$|$+$|極小点|
|$(0,-1)$|$+$|$+$|極小点|

　  
　  
以上の停留点及びx,yに関する対称性から$g(x,y)$に関する外形は下の通り

![](0704-0.png)
　  
　  
　  
### (2)
---
### Point!

- 陰関数微分

$$
\frac{dx}{dy} = -\frac{f_y}{f_x}
$$

　  
　  
$$
\scriptsize
\begin{aligned}
\text{--[導出]--} \\
f(x,y(x)) = 0 \\
\\
\frac{df}{dx} = 0\\
\frac{1}{dx}\bigl( f_xdx + f_ydy \bigr) = 0\\
f_x + f_y\frac{dy}{dx}=0 \\
\frac{dx}{dy} = -\frac{f_y}{f_x}
\end{aligned}
$$

---

$$
\begin{aligned}
\frac{dx}{dy} = -\frac{g_y}{g_x}
    &= \frac{x^3+y^2x-x}{x^2y+y^3+y} \\
    &= \frac{x(x^2+y^2-1)}{y(y^2+x^2+1)}\\
    \\
\frac{dy}{dx} = -\frac{g_x}{g_y}
    &= \frac{y(y^2+x^2+1)}{x(x^2+y^2-1)}\\
\end{aligned}
$$

　  
　  
### (3)
---
### Point!

- $z=f(x,y)$の$z=k$における断面

$
\rightarrow f(x.y) = k
$

- 断面の概形判断
 + $\displaystyle \frac{dy}{dx}=0$ : 水平な接線が引ける
 + $\displaystyle \frac{dx}{dy}=0$ : 垂直な接線が引ける

---

　  
　  
$g(x,y)$はx,yに関して対称であるので$x\geqq0,y\geqq0$のときを考える

ところで

$
\begin{aligned}
\frac{dy}{dx}=0 & \Leftrightarrow \frac{x(x^2+y^2-1)}{y(y^2+x^2+1)}\\
                & \Leftrightarrow y=0
\end{aligned}
$

$
\begin{aligned}
\frac{dy}{dx}=0 & \Leftrightarrow \frac{y(y^2+x^2+1)}{x(x^2+y^2-1)}\\
                & \Leftrightarrow
    \begin{cases}
        x=0  \\
        x^2+y^2=1
    \end{cases}
\end{aligned}
$

このように変換できるのでこの条件を$g(x,y)=k$に代入する

- $y=0 (\Leftrightarrow\frac{dy}{dx}=0)$を代入
　  
　  

$$
\begin{aligned}
g = k(y=0) & \Leftrightarrow x^4-2x^2 = k\,\,(y=0) \\
      & \Leftrightarrow  
      x=\pm{\sqrt{1\pm{\sqrt{1+k}}}},\,\,y=0
\end{aligned}
$$


- $ \begin{cases}x=0  \\x^2+y^2=1\end{cases}(\Leftrightarrow\frac{dx}{dy}=0)$を代入
      
      

$$
\begin{aligned}
g &= k\Biggr(\begin{cases}x=0  \\x^2+y^2=1\end{cases} \Biggl) \\
& \Leftrightarrow 
\begin{cases}
        y^4 + 2y^2 = k\,\,(x=0)  \\
        -2x^2 + 2y^2 + 1 = k\,\,(x^2+y^2=1)
\end{cases}
  \\
 &\Leftrightarrow   
      \begin{cases}
        y=\pm{\sqrt{-1\pm{\sqrt{1+k}}}},\,\,(x=0) \\
        y=\pm{\frac{1}{2}\sqrt{1+k}},\,\,(x^2+y^2=1)
      \end{cases}
\end{aligned}
$$

以上から各kを調べると各kでの断面は次のようになる
　  
　  
　  
![](0704-01.jpg)























